﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComprobarPuertas : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "puertaBlanca")
        {
            GetComponent<AudioSource>().clip = (AudioClip)Resources.Load("collision");
            GetComponent<AudioSource>().Play();
            if (player.GetComponent<movimiento>().white == false)
            {
                collision.gameObject.SetActive(false);

            }
            else if (player.GetComponent<movimiento>().white == true)
            {
                player.GetComponent<BarraDeVida>().QuitarVida();
                Debug.Log("me quitan vida 1");
            }
        }

        if(collision.gameObject.tag == "puertaNegra")
        {
            GetComponent<AudioSource>().clip = (AudioClip)Resources.Load("collision");
            GetComponent<AudioSource>().Play();
            if (player.GetComponent<movimiento>().white == true)
            {
                collision.gameObject.SetActive(false);

            }
            else if (player.GetComponent<movimiento>().white == false)
            {
                player.GetComponent<BarraDeVida>().QuitarVida();
                Debug.Log("me quitan vida 2");
            }
        }
    }
}

