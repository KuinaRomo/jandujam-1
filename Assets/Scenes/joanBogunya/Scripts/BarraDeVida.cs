﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour {

    public Image Barra;
    public Text VidaActualJugador;
    public float vidaMaxima = 100;
    public float vidajugador;
    

	// Use this for initialization
	void Start () {

        vidajugador = vidaMaxima;

        Barra.fillAmount = vidajugador / vidaMaxima;

    }

    private void Update()
    {
        VidaActualJugador.text = string.Format("{0:N0}", vidajugador + string.Concat("%"));
    }

    public void QuitarVida()
    {
        
        vidajugador -= 30;

        Barra.fillAmount = vidajugador / vidaMaxima;

        if(vidajugador <= 0)
        {
            GetComponent<AudioSource>().clip = (AudioClip)Resources.Load("defeat");
            GetComponent<AudioSource>().Play();
            gameObject.GetComponent<Transform>().position = gameObject.GetComponent<movimiento>().pos;

            vidajugador = vidaMaxima;

            Barra.fillAmount = vidajugador / vidaMaxima;

        }
        
    }



}
