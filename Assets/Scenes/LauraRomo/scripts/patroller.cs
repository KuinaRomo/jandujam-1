﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class patroller : MonoBehaviour {

    [SerializeField] bool toRight;
    [SerializeField] float move;

	// Use this for initialization
	void Start () {
        if (toRight) {
            GetComponent<Animator>().SetBool("right", true);
        }
        else {
            GetComponent<Animator>().SetBool("right", false);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (toRight) {
            transform.position = new Vector2(transform.position.x - move, transform.position.y);
        }
        else {
            transform.position = new Vector2(transform.position.x + move, transform.position.y);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.tag == "wall") {
            toRight = !toRight;
            if (toRight) {
                GetComponent<Animator>().SetBool("right", true);
            }
            else {
                GetComponent<Animator>().SetBool("right", false);
            }
        }
    }
}
