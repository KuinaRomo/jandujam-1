﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour {

    [SerializeField] private float speed = 5f;
    private float direction;
    private float jump;
    private bool inFloor;
    public bool white;
    private bool endJump;
    private float contador;
    private float xToJump;
    private float yToJump;
    private int times;
    private int directionJump;
    private int directionWalk;
    private int contadorInFloor;
    private bool desactivated;
    [SerializeField] private float xHighJump;
    [SerializeField] private float yHighJump;
    [SerializeField] private float xLongJump;
    [SerializeField] private float yLongJump;
    [SerializeField] private GameObject blackParticles;
    [SerializeField] private GameObject whiteParticles;
    public Vector3 pos;

    // Use this for initialization
    void Start () {
        inFloor = false;
        white = true;
        endJump = true;
        desactivated = false;
        contador = 0;
        times = 0;
        directionJump = 1;
        directionWalk = 1;
        contadorInFloor = 0;
        pos = gameObject.transform.position;
    }

    private void Update() {
        if(direction < 0) {
            directionJump = -1;
        }
        else if(direction > 0) {
            directionJump = 1;
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if(GetComponent<Rigidbody2D>().velocity.y == 0 && !desactivated) {
            GetComponent<Animator>().SetBool("whiteIzq", false);
            GetComponent<Animator>().SetBool("whiteDer", false);
            GetComponent<Animator>().SetBool("blackIzq", false);
            GetComponent<Animator>().SetBool("blackDer", false);
            whiteParticles.SetActive(false);
            blackParticles.SetActive(false);
            desactivated = true;
        }

        direction = Input.GetAxis("Horizontal");
        if (inFloor) {
            transform.position = new Vector2(transform.position.x + speed * direction, transform.position.y);
            if(direction > 0) {
                GetComponent<Animator>().SetBool("walkLeft", false);
                GetComponent<Animator>().SetBool("walkRight", true);
                directionWalk = 1;
            }
            else if(direction < 0) {
                GetComponent<Animator>().SetBool("walkRight", false);
                GetComponent<Animator>().SetBool("walkLeft", true);
                directionWalk = -1;
            }
            else {
                GetComponent<Animator>().SetBool("walkLeft", false);
                GetComponent<Animator>().SetBool("walkRight", false);
                if(directionWalk == 1) {
                    GetComponent<Animator>().SetBool("right", false);
                }
                else {
                    GetComponent<Animator>().SetBool("right", true);
                }
            }
        }
        
        //Salto alto
        if (Input.GetKeyDown(KeyCode.Space)){
            if (inFloor && !endJump && GetComponent<Rigidbody2D>().velocity.y == 0) {
                desactivated = false;
                if (white) {
                    saltoAlto();
                }

                else {
                    saltoLargo();
                }
            }
            else {
                Debug.Log("No puedo saltar");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "floor") {
            contadorInFloor++;
            inFloor = true;
            endJump = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.tag == "floor") {
            if(contadorInFloor == 1) {
                inFloor = false;
                endJump = true;
            }
            contadorInFloor--;
        }
    }

    private void saltoAlto() {
        GetComponent<AudioSource>().clip = (AudioClip)Resources.Load("lightjump");
        GetComponent<AudioSource>().Play();
        if (directionJump == -1) {
            GetComponent<Animator>().SetBool("whiteDer", true);
        }
        else if (directionJump == 1){
            GetComponent<Animator>().SetBool("whiteIzq", true);
        }

        GetComponent<Rigidbody2D>().AddForce(new Vector2(xHighJump * directionJump, yHighJump));
        endJump = true;
        white = !white;
        changeParticle();
    }

    private void saltoLargo() {
        GetComponent<AudioSource>().clip = (AudioClip)Resources.Load("darkjump");
        GetComponent<AudioSource>().Play();
        if (directionJump == -1){
            GetComponent<Animator>().SetBool("blackDer", true);
        }
        else if (directionJump == 1){
            GetComponent<Animator>().SetBool("blackIzq", true);
        }
        GetComponent<Animator>().SetBool("blackJump", true);
        GetComponent<Rigidbody2D>().AddForce(new Vector2(xLongJump * directionJump, yLongJump));
        endJump = true;
        white = !white;
        changeParticle();
    }

    private void changeParticle() {
        if (!white) {
            whiteParticles.SetActive(true);
        }
        else {
            blackParticles.SetActive(true);
        }
    }
}
