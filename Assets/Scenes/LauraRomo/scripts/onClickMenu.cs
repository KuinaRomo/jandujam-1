﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class onClickMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void nuevaPartida() {
        SceneManager.LoadScene("JoanBogunya");
    }

    public void goToCredits() {
        SceneManager.LoadScene("Credits");
    }

    public void exit() {
        Application.Quit();
    }
}
