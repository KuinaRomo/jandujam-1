﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killEnemies : MonoBehaviour {
    [SerializeField] bool auraWhite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.tag == "whiteEnemy") {
            if (auraWhite) {
                Debug.Log("Herir personaje");
            }
            else {
                Destroy(collision.gameObject);
            }
        }

        if (collision.gameObject.tag == "blackEnemy") {
            if (!auraWhite) {
                Debug.Log("Herir personaje");
            }
            else {
                Destroy(collision.gameObject);
            }
        }
    }
}
